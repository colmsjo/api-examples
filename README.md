Gizur API examples
=================

This repository contains some basic examples on howto use the Gizur REST API.

Copy config.js.sample to config.js and update with your credentials before running.


Javascript
=========

Dependencies
-----------

The followinig node modules are used:

 * jQuery 
 * jsdom 
 * xmlhttprequest 
 * optimist

```
npm install -g jsdom xmlhttprequest jQuery optimist
```

Howto run
--------

1. Copy config.js.sample to config.js and update with your credentials
1. Run the example like this: `./exampleX.js`


ToDo
----

Develop a package.json file. See dependencies above instead.


PHP
===


Howto run
--------

1. Copy config.inc.php.sample to config.inc.php and update with your credentials
1. Run the example like this: `./exampleX.php`

