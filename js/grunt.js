/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    meta: {
      version: '0.1.0',
      banner: '/*! Gizur API Examples - v<%= meta.version %> - ' +
        '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
        '* http://gizur.com/\n' +
        '* Copyright (c) <%= grunt.template.today("yyyy") %> ' +
        'Gizur AB; Licensed MIT */'
    },
    lint: {
      files: ['grunt.js', '*.js', 'lib/helpers.js', 'test/**/*.js']
    },
    test: {
      files: ['test/**/*.js']
    },
    concat: {
      dist: {
        src: ['<banner:meta.banner>', '<file_strip_banner:lib/*.js>'],
        dest: 'dist/gizur-exmaples.js'
      }
    },
    min: {
      dist: {
        src: ['<banner:meta.banner>', '<config:concat.dist.dest>'],
        dest: 'dist/gizur-examples.min.js'
      }
    },
    watch: {
      files: '<config:lint.files>',
      tasks: 'lint test'
    },
    jshint: {
      options: {
        curly: true,
        eqeqeq: true,
        immed: true,
        latedef: true,
        newcap: true,
        noarg: true,
        sub: true,
        undef: true,
        boss: true,
        eqnull: true,
        node: true
      },
      globals: {
        jQuery: true,
        exports: true
      }
    },
    uglify: {}
  });


  // Custom task for generating docs
  // -------------------------------

  grunt.registerTask('docco', 'Generating docs...', function() {
    var docco = grunt.helper('docco');
    grunt.log.write(docco);
  });

  // The helper doing the work
  grunt.registerHelper('docco', function() {
    var myTerminal = require("child_process").exec;

    myTerminal("rm docs/*");
    myTerminal("docco example1.js lib/helpers.js");
    myTerminal("docco lib/helpers.js");
  });


  // Default task.
  // -------------------------------

  grunt.registerTask('default', 'lint docco');

};
