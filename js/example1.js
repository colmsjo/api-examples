#!/usr/bin/env node


// example1.js
//------------------------------
//
// 2012-10-12, Jonas Colmsjö
//
// Copyright 2012 Gizur AB 
//
// Examples for the Gizur REST API
//
// dependencies: npm install jsdom xmlhttprequest jQuery optimist
//
// Using Google JavaScript Style Guide - http://google-styleguide.googlecode.com/svn/trunk/javascriptguide.xml
//
// The code is commented in 'docco style' - http://jashkenas.github.com/docco/ 
//
//------------------------------



(function () {

    'use strict';

    // Includes
    // ================


    // Node modules
    // ------------

    var jQuery  = require('jQuery'),
        $       = jQuery,
        argv    = require('optimist')
                    .usage('Usage: ./example1.js')
                    .argv,

        // Local libraries
        // ---------------

        helpers = require('./lib/helpers.js'),
        sha     = require('./lib/sha256.js'),

       // Credentials for logging in to Gizur
        config  = require('./config.js').Config;


    // set logging level (see helpers.js)
    // Set to logging.warn to remove debugging output
    global.logging.threshold  = global.logging.debug;



    // Functions
    //==============


    // getTickets
    //-------------------------------------------------------------------------------------------------
    //

    function getTickets(user, password) {

        // Local variables
        var method    = 'GET',
            url       = config.GIZURCLOUD_API_URL + '/HelpDesk/damaged',

            // Calculate the signature to use
//            signature = global.sign('HelpDesk', method, config.GIZURCLOUD_API_KEY, config.GIZURCLOUD_SECRET_KEY, 3600),
            signature = global.sign('HelpDesk', method, config.GIZURCLOUD_API_KEY, config.GIZURCLOUD_SECRET_KEY, 3600*2),
            request;

        // Usefull for debuggning
        global.logDebug('getTickets - url:' + url + ' user:' + user + " password:" + password + " timestamp:" + signature.timestamp +
                 " API_KEY:" + config.GIZURCLOUD_API_KEY + " signature:" + signature.base64 + " salt:" + signature.salt);

        // Make the API call
        request = $.ajax({

            url: url,

            type: method,

            headers: {
                'x_username':          user,
                'x_password':          password,

                //ISO 8601 format must be sync with the server with max -10 seconds error
                'x_timestamp':          signature.timestamp,

                // The API Key is fetched from config.js
                'x_gizurcloud_api_key': config.GIZURCLOUD_API_KEY,

                // Base64 encoded signature 
                'x_signature':          signature.base64,

                'x_unique_salt':        signature.salt,

                'accept': 'text/json'
              },


            // If the call was successfull
            success: function (data) {
                global.logDebug('getTickets: Yea, it worked...' + JSON.stringify(data));
            },

            // The call failed
            error: function (data) {
                global.logErr('getTickets: Shit hit the fan...' + JSON.stringify(data));

            }
        });

        // Return JSON object
        return request;

    }


    // Main
    //=========

    // Get the tickets
    // username and password is fetched from config.js
    getTickets(config.USER, config.PASSWORD);

}());
