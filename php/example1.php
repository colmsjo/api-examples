#!/usr/bin/env php

<?php

// example1.php
//------------------------------
// 2012-10-12, Jonas Colmsjö
// 
// Copyright 2012 Gizur AB 
//
// Examples for the Gizur REST API
//
// Coding standard - http://pear.php.net/manual/en/standards.php
//
// The code is commented in 'docco style' - http://jashkenas.github.com/docco/ 
//
//---------------------------


// Includes
// ========

// Curl library
require_once 'lib/curl/curl.php';

// Get credentials 
require_once 'config.inc.php';


// Functions
// =========

function getAssetList(){
    global $credentials, $url;

    $model = 'Assets';

    echo " Getting Asset List..." . PHP_EOL;        

    date_default_timezone_set("Europe/Stockholm");

    $params = array(
                'Verb'          => 'GET',
                'Model'         => $model,
                'Version'       => API_VERSION,
                'Timestamp'     => date("c"),
                'KeyID'         => GIZURCLOUD_API_KEY,
                'UniqueSalt'    => uniqid()
    );

    // Sorg arguments
    ksort($params);

    // Generate string for sign
    $string_to_sign = "";
    foreach ($params as $k => $v)
        $string_to_sign .= "{$k}{$v}";

    // HMAC hash to encode
    $hash = hash_hmac('SHA256', $string_to_sign, GIZURCLOUD_SECRET_KEY, 1);

    // Generate signature
    $signature = base64_encode(hash_hmac('SHA256', $string_to_sign, GIZURCLOUD_SECRET_KEY, 1));

    // Print the signature stuff
    echo "Parms"            . $params         . PHP_EOL;
    echo "String to sign: " . $string_to_sign . PHP_EOL;
    echo "HMAC hash: "      . $hash           . PHP_EOL;
    echo "Signature: "      . $signature      . PHP_EOL;

    // login using each credentials
    foreach($credentials as $username => $password){   


        // Create a Curl instance
        $rest = new Curl;

        // Set the headers
        $rest->headers['X_USERNAME']           = $username;
        $rest->headers['X_PASSWORD']           = $password;
        $rest->headers['X_TIMESTAMP']          = $params['Timestamp'];
        $rest->headers['X_UNIQUE_SALT']        = $params['UniqueSalt'];
        $rest->headers['X_SIGNATURE']          = $signature;                   
        $rest->headers['X_GIZURCLOUD_API_KEY'] = GIZURCLOUD_API_KEY;

        // Make the call
        $response = $rest->get($url . $model);

        // Decode the json returned
        $responseJSON = json_decode($response);

        //check if response is valid
        if (isset($responseJSON->success)){
            echo "Yea it worked: " . $response . PHP_EOL;
        } else {
            echo "Shit hit the fan: " .  $response . PHP_EOL;
        }

        unset($rest);
    } 
}


// Main
// ====

// Get a asset list
getAssetList();

?>
